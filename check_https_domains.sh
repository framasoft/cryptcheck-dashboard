#!/bin/bash
P=$(pwd)
A=''
AAAA=''
export PATH=/usr/local/rbenv/versions/2.3.8-cryptcheck/bin/:$PATH
cd /cryptcheck || exit 1
echo -n "{ " >>"$P/public/result.json"
echo -n "[ " >>"$P/public/resultv2.json"
for i in $DOMAINS; do
    printf "Analyzing %-32s " "$i"
    GRADE=$(bin/cryptcheck https "$i" --json --quiet | jq -r '.[0] | .grade')
    [[ -z $(dig +short "$i") ]] && A="false" || A="true"
    [[ -z $(dig +short "$i" AAAA) ]] && AAAA="false" || AAAA="true"
    echo "$GRADE"
    echo "$i: $GRADE" >>"$P/public/result.yml"
    echo -n "  \"$i\": \"$GRADE\", " >>"$P/public/result.json"
    echo -n "{ \"site\": \"$i\", \"cryptcheck\": \"$GRADE\", \"dns\": { \"A\": $A, \"AAAA\": $AAAA } }, " >>"$P/public/resultv2.json"
done
echo "}" >>"$P/public/result.json"
sed -i -e "s@, }@ }@" "$P/public/result.json"

echo "]" >>"$P/public/resultv2.json"
sed -i -e "s@, ]@ ]@" "$P/public/resultv2.json"
